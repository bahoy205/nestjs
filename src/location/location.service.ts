import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class LocationService {
  private readonly apiKey: string;
  private readonly logger = new Logger(LocationService.name);

  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {
    this.apiKey = this.configService.get<string>('GOOGLE_API_KEY');
    if (!this.apiKey) {
      throw new Error('GOOGLE_API_KEY is not defined in the environment variables');
    }
  }

  async getCoordinates(): Promise<{ latitude: number; longitude: number; mapUrl: string; timestamp: string }> {
    const url = `https://www.googleapis.com/geolocation/v1/geolocate?key=${this.apiKey}`;

    const requestBody = {
      considerIp: true, // ใช้ IP address ในการประมาณตำแหน่ง
    };

    try {
      this.logger.debug(`Sending request to URL: ${url}`);
      const response = await firstValueFrom(this.httpService.post(url, requestBody));
      this.logger.debug(`Response received: ${JSON.stringify(response.data)}`);

      const data = response.data;

      if (data && data.location) {
        const latitude = data.location.lat;
        const longitude = data.location.lng;
        const accuracy = data.accuracy; // ตรวจสอบค่า accuracy
        this.logger.debug(`Location accuracy: ${accuracy} meters`);

        if (accuracy > 100) { // กำหนด threshold สำหรับความแม่นยำ
          this.logger.warn('Location accuracy is too low');
        }

        const mapUrl = `https://www.google.com/maps?q=${latitude},${longitude}`;
        const timestamp = new Date().toLocaleString(); // แปลง timestamp ให้เป็นรูปแบบที่อ่านง่าย

        return { latitude, longitude, mapUrl, timestamp };
      } else {
        throw new HttpException(
          'Unable to retrieve location data',
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    } catch (error) {
      this.logger.error(`Failed to fetch coordinates: ${error.message}`);
      throw new HttpException(
        `Failed to fetch coordinates: ${error.message}`,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
