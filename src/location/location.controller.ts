import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { LocationService } from './location.service';
import { CreateLocationDto } from './dto/create-location.dto';
import { UpdateLocationDto } from './dto/update-location.dto';

@Controller('location')
export class LocationController {
  constructor(private readonly locationService: LocationService) {}

  @Post()
  async saveLocation(@Body() body: { latitude: number; longitude: number }) {
    const { latitude, longitude } = body;
    // คุณสามารถเก็บหรือประมวลผลข้อมูลที่นี่ได้
    return { message: 'Location received', latitude, longitude };
  }

  @Get()
  async getLocation() {
    const coordinates = await this.locationService.getCoordinates();
    return coordinates;
  }

//   @Get(':id')
//   findOne(@Param('id') id: string) {
//     return this.locationService.findOne(+id);
//   }

//   // @Patch(':id')
//   // update(@Param('id') id: string, @Body() updateLocationDto: UpdateLocationDto) {
//   //   return this.locationService.update(+id, updateLocationDto);
//   // }

//   @Delete(':id')
//   remove(@Param('id') id: string) {
//     return this.locationService.remove(+id);
//   }
}
