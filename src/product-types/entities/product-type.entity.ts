import { Entity, PrimaryGeneratedColumn, Column, Index, OneToMany } from 'typeorm';

@Entity()
export class ProductType {
    @PrimaryGeneratedColumn()
    productTypeId: number;
  
    @Column()
    productTypeName: string;
  
}
