import { Injectable, ConflictException,NotFoundException } from '@nestjs/common';
import { CreateProductTypeDto } from './dto/create-product-type.dto';
import { UpdateProductTypeDto } from './dto/update-product-type.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductType } from './entities/product-type.entity';
import { Repository } from 'typeorm';


@Injectable()
export class ProductTypesService {
  constructor(
    @InjectRepository(ProductType)
    private productTypeRepository: Repository<ProductType>,
  ) {}

  async create(createProductTypeDto: CreateProductTypeDto): Promise<ProductType> {
    const existingProductType = await this.productTypeRepository.findOne({ where: { productTypeName: createProductTypeDto.productTypeName } });
    if (existingProductType) {
      throw new ConflictException(`ProductType with name "${createProductTypeDto.productTypeName}" already exists`);
    }

    const productType = this.productTypeRepository.create(createProductTypeDto);
    return this.productTypeRepository.save(productType);
  }

  async findAll(){
    return this.productTypeRepository.find();
  }

  async findOne(productTypeId : number): Promise<ProductType> {
    const productType = await this.productTypeRepository.findOneBy({ productTypeId });
    if (!productType) {
      throw new NotFoundException(`ProductType with ID ${productTypeId} not found`);
    }
    return productType;
  }

  async update(id: number, updateProductTypeDto: UpdateProductTypeDto): Promise<ProductType> {
    await this.productTypeRepository.update(id, updateProductTypeDto);
    return this.findOne(id);
  }

  async remove(id: number): Promise<void> {
    const result = await this.productTypeRepository.delete(id);
    if (result.affected === 0) {
      throw new NotFoundException(`ProductType with ID ${id} not found`);
    }
  }
}
