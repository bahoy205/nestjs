import { Controller, Get, Post, Body, Patch, Param, Delete , ValidationPipe,Res ,HttpException ,HttpStatus  } from '@nestjs/common';
import { OrderService } from './orders.service';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { Order } from './entities/order.entity';
import { Response } from 'express';

@Controller('orders')
export class OrdersController {
  constructor(private readonly orderService: OrderService) {}

  @Post()
  async createOrder(@Body(ValidationPipe) createOrderDto: CreateOrderDto) {
    return this.orderService.create(createOrderDto);
  }

  @Get()
  findAll() {
    return this.orderService.findAll();
  }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.ordersService.findOne(+id);
  // }

  @Get('/order/:id')
  async findOrdersByProductId(@Param('productId') productId: number) {
    return this.orderService.findOrdersByProductId(productId);
  }

  @Get('by-product-type/:productType')
  async findOrderIdsByProductType(@Param('productType') productType: string): Promise<Order[]> {
    const orders = await this.orderService.findOrdersByProductType(productType);
    return orders;
  }

  @Get('/:userId')
  async getOrdersByUser(@Param('userId') userId: number): Promise<Order[]> {
    return this.orderService.findOrdersByUser(userId);
  }
  

  // @Get('pdf')
  // async getOrdersPdf(@Res() res: Response) {
  //   const pdfBuffer = await this.orderService.generatePdf();
  //   res.setHeader('Content-Type', 'application/pdf');
  //   res.setHeader('Content-Disposition', 'attachment; filename=orders.pdf');
  //   res.send(pdfBuffer);
  // }

  @Get(':userId/pdf')
  async getOrderPdf(@Param('userId') userId: number, @Res() res: Response): Promise<void> {
    try {
      const pdfData = await this.orderService.generateOrderPdfByUserId(userId);
      res.setHeader('Content-Type', 'application/pdf');
      res.setHeader('Content-Disposition', 'attachment; filename=orders_${userId}.pdf');
      res.send(pdfData);
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateOrderDto: UpdateOrderDto) {
    return this.orderService.update(+id, updateOrderDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.orderService.remove(+id);
  }
}
