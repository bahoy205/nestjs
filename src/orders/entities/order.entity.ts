import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, JoinColumn, CreateDateColumn } from 'typeorm';
import { User } from 'src/users/entities/user.entity';
import { OrderItem } from 'src/order-item/entities/order-item.entity';

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user) => user.orders)
  @JoinColumn({ name: 'userId' }) // Ensures that the foreign key column is named 'userId'
  user: User;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.order, { cascade: true }) // 'cascade: true' means related OrderItems are automatically persisted
  items: OrderItem[];

  @Column() // Assuming you want to specify precision and scale for decimal
  totalPrice: number;

  @CreateDateColumn({ type: 'timestamp' })
  orderDate: Date;
}
