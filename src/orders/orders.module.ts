import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderService } from './orders.service';
import { OrdersController } from './orders.controller';
import { Order } from './entities/order.entity';
import { Product } from 'src/products/entities/product.entity';
import { User } from 'src/users/entities/user.entity';
import { OrderItem } from 'src/order-item/entities/order-item.entity';
// import { OrderItem } from 'sr';
// import { AuthService } from '../auth/auth.service'; // Import AuthService

@Module({
  imports: [TypeOrmModule.forFeature([Order, Product ,User, OrderItem])],
  exports: [TypeOrmModule, OrderService], // Export OrderService
  controllers: [OrdersController],
  providers: [OrderService], // Add AuthService to providers
})
export class OrdersModule {}
