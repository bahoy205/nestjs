import { Injectable, BadRequestException ,NotFoundException} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Order } from './entities/order.entity';
// import { OrderItem } from './entities/order-item.entity';
import { OrderItem } from 'src/order-item/entities/order-item.entity';
import { Product } from 'src/products/entities/product.entity';
import { User } from 'src/users/entities/user.entity';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { createWriteStream } from 'fs';
import fs from 'fs';
import * as PDFDocument from 'pdfkit';

@Injectable()
export class OrderService {
  constructor(
    @InjectRepository(Order)
    private orderRepository: Repository<Order>,
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(OrderItem)
    private orderItemRepository: Repository<OrderItem>,
  ) {}

  async create(createOrderDto: CreateOrderDto): Promise<Order> {
    const { userId, products } = createOrderDto;
  
    const user = await this.userRepository.findOne({
      where: { userId },
    });
  
    if (!user) {
      throw new BadRequestException('User not found');
    }
  
    const orderItems: OrderItem[] = [];
    let totalPrice = 0;
  
    for (const { productId, quantity } of products) {
      const product = await this.productRepository.findOne({
        where: { productId },
      });
  
      if (!product) {
        throw new BadRequestException(`Product with ID ${productId} not found`);
      }
  
      if (product.amount < quantity) {
        throw new BadRequestException(`Not enough stock for product with ID ${productId}`);
      }
  
      product.amount -= quantity;
      await this.productRepository.save(product);
  
      const orderItem = new OrderItem();
      orderItem.product = product;
      orderItem.quantity = quantity;
      orderItem.price = product.price * quantity;
      orderItems.push(orderItem);
  
      totalPrice += product.price * quantity;
    }
  
    const order = new Order();
    order.user = user;
    order.items = orderItems;
    order.totalPrice = totalPrice;
    order.orderDate = new Date(); // กำหนดค่า orderDate
  
    return this.orderRepository.save(order);
  }
  
  // async findAll() {
  //   return this.orderRepository.find({ 
  //     relations: ['items', 'items.product', 'user'] 
  //   });
  // }
  
  // async findAll(): Promise<Order[]> {
  //   return this.orderRepository.find({ 
  //     relations: ['items', 'items.product', 'user'] 
  //   });
  // }

  // async generatePdf(): Promise<Buffer> {
  //   return new Promise(async (resolve, reject) => {
  //     const orders = await this.findAll();
  //     const doc = new PDFDocument();

  //     const buffers: Buffer[] = [];
  //     doc.on('data', buffers.push.bind(buffers));
  //     doc.on('end', () => {
  //       const pdfData = Buffer.concat(buffers);
  //       resolve(pdfData);
  //     });

  //     doc.fontSize(25).text('Orders Report', { align: 'center' });

  //     orders.forEach((order) => {
  //       doc.moveDown();
  //       doc.fontSize(15).text(`Order #${order.id}`, { align: 'left' });
  //       doc.fontSize(12).text(`User: ${order.user.username}`);
  //       order.items.forEach(item => {
  //         doc.text(`- ${item.product.productName}: ${item.quantity}`);
  //       });
  //     });

  //     doc.end();
  //   });
  // }

  async findAll(): Promise<OrderItem[]> {
    return this.orderItemRepository.find({ 
      relations: ['order', 'product', 'order.user'] 
    });
  }

  // async generatePdf(): Promise<Buffer> {
  //   return new Promise(async (resolve, reject) => {
  //     const orderItems = await this.findAll();
  //     const doc = new PDFDocument();

  //     const buffers: Buffer[] = [];
  //     doc.on('data', buffers.push.bind(buffers));
  //     doc.on('end', () => {
  //       const pdfData = Buffer.concat(buffers);
  //       resolve(pdfData);
  //     });

  //     doc.fontSize(25).text('Order Items Report', { align: 'center' });

  //     orderItems.forEach((item) => {
  //       doc.moveDown();
  //       doc.fontSize(15).text(`Order Item #${item.id}`, { align: 'left' });
  //       doc.fontSize(12).text(`Product: ${item.product.productName}`);
  //       doc.text(`Quantity: ${item.quantity}`);
  //       doc.text(`Price: ${item.price}`);
  //       doc.text(`Order ID: ${item.order.id}`);
  //       doc.text(`User: ${item.order.user.username}`);
  //     });

  //     doc.end();
  //   });
  // }

  async findOne(id: number) {
    return this.orderRepository.findOne({ where: { id }, relations: ['product'] });
  }

  async findOrdersByProductId(productId: number) {
    return this.orderRepository
      .createQueryBuilder('order')
      .leftJoinAndSelect('order.product', 'product') 
      .where('product.id = :productId', { productId })
      .getMany();
  }

  async findOrdersByProductType(productType: string): Promise<Order[]> {
    const orders = await this.orderRepository
      .createQueryBuilder('order')
      .innerJoinAndSelect('order.items', 'item')
      .innerJoinAndSelect('item.product', 'product')  
      .innerJoinAndSelect('order.user', 'user') 
      .where('product.productType = :productType', { productType })
      .getMany();
    return orders;
  }

  async findOrdersByUser(userId: number): Promise<Order[]> {
    const orders = await this.orderRepository
      .createQueryBuilder('order')
      .innerJoinAndSelect('order.items', 'item')
      .innerJoinAndSelect('item.product', 'product')
      .where('order.userId = :userId', { userId })
      .getMany();
    return orders;
  }

  async generateOrderPdfByUserId(userId: number): Promise<Buffer> {
    const orders = await this.findOrdersByUser(userId);
  
    if (orders.length === 0) {
      throw new NotFoundException('No orders found for this user.');
    }
  
    const doc = new PDFDocument();
    const buffers: Buffer[] = [];
  
    doc.fontSize(25).text(`Orders for User ID: ${userId}`, { align: 'center' });
    doc.moveDown();
  
    orders.forEach(order => {
      const totalPrice = typeof order.totalPrice === 'number' ? order.totalPrice.toFixed(2) : 'N/A'; // Check if totalPrice is a number before calling .toFixed()
      doc.fontSize(20).text(`Order ID: ${order.id}, Total Price: ${totalPrice}, Order Date: ${formatDate(order.orderDate)}`);
      order.items.forEach(item => {
        const price = typeof item.price === 'number' ? item.price.toFixed(2) : 'N/A'; // Check if item.price is a number before calling .toFixed()
        doc.fontSize(15).text(`- Product: ${item.product.productName}, Quantity: ${item.quantity}, Price: ${price}`);
      });
      doc.moveDown();
    });
  
    doc.end();
  
    return new Promise<Buffer>((resolve, reject) => {
      doc.on('data', buffers.push.bind(buffers));
      doc.on('end', () => {
        const pdfData = Buffer.concat(buffers);
        resolve(pdfData);
      });
    });
  }


  update(id: number, updateOrderDto: UpdateOrderDto) {
    return `This action updates a #${id} order`;
  }

  remove(id: number) {
    return `This action removes a #${id} order`;
  }
}

function formatDate(date: Date): string {
  return date.toISOString().split('T')[0]; // Format date as YYYY-MM-DD
}