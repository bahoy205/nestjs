import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { HttpModule } from '@nestjs/axios';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { ProductsModule } from './products/products.module';
import { OrdersModule } from './orders/orders.module';
import { User } from './users/entities/user.entity';
import { Product } from './products/entities/product.entity';
import { Order } from './orders/entities/order.entity';
import { ProductTypesModule } from './product-types/product-types.module';
import { ProductType } from './product-types/entities/product-type.entity';
// import { OrderItem } from './orders/entities/order-item.entity';
import { LocationModule } from './location/location.module';
// import { LocationService } from './location/location.service';
import { OrderItemModule } from './order-item/order-item.module';
import { OrderItem } from './order-item/entities/order-item.entity';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'mynestjs',
      entities: [User, Product, Order, , ProductType, OrderItem],
      synchronize: true,
      retryAttempts: 5,
      retryDelay: 3000,
    }),
    HttpModule,
    UsersModule,
    AuthModule,
    ProductsModule,
    OrdersModule,
    ProductTypesModule,
    LocationModule,
    OrderItemModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}