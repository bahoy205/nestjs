import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateOrderItemDto } from './dto/create-order-item.dto';
import { UpdateOrderItemDto } from './dto/update-order-item.dto';
import { Order } from 'src/orders/entities/order.entity';
import { Product } from 'src/products/entities/product.entity';
import { User } from 'src/users/entities/user.entity';
import { OrderItem } from './entities/order-item.entity';
import * as PDFDocument from 'pdfkit';

@Injectable()

export class OrderItemService {
  constructor(
    @InjectRepository(Order)
    private orderRepository: Repository<Order>,
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(OrderItem)
    private orderItemRepository: Repository<OrderItem>,
  ) {}

  create(createOrderItemDto: CreateOrderItemDto) {
    return 'This action adds a new orderItem';
  }

  async findAll(): Promise<OrderItem[]> {
    return this.orderItemRepository.find({ 
      relations: ['order', 'product', 'order.user'] 
    });
  }

  async generatePdf(): Promise<Buffer> {
    return new Promise(async (resolve, reject) => {
      const orderItems = await this.findAll();
      const doc = new PDFDocument();

      const buffers: Buffer[] = [];
      doc.on('data', buffers.push.bind(buffers));
      doc.on('end', () => {
        const pdfData = Buffer.concat(buffers);
        resolve(pdfData);
      });

      doc.fontSize(25).text('Order Items Report', { align: 'center' });

      orderItems.forEach((item) => {
        doc.moveDown();
        doc.fontSize(15).text(`Order Item #${item.id}`, { align: 'left' });
        doc.fontSize(12).text(`Product: ${item.product.productName}`);
        doc.text(`Quantity: ${item.quantity}`);
        doc.text(`Price: ${item.price}`);
        doc.text(`Order ID: ${item.order.id}`);
        doc.text(`User: ${item.order.user.username}`);
      });

      doc.end();
    });
  }

  
}



  // async getOrderItemsByUserId(userId: number): Promise<OrderItem[]> {
  //   return await this.orderItemRepository.find({
  //     where: { userId: userId },
  //   });
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} orderItem`;
  // }

  // update(id: number, updateOrderItemDto: UpdateOrderItemDto) {
  //   return `This action updates a #${id} orderItem`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} orderItem`;
  // }


