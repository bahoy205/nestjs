import { Module } from '@nestjs/common';
import { OrderItemService } from './order-item.service';
import { OrderItemController } from './order-item.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from 'src/orders/entities/order.entity';
import { Product } from 'src/products/entities/product.entity';

import { OrderItem } from './entities/order-item.entity';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Order, Product ,OrderItem,User])],
  exports: [TypeOrmModule, OrderItemService], // Export OrderService
  controllers: [OrderItemController],
  providers: [OrderItemService],
})
export class OrderItemModule {}
