import { Controller, Get, Post, Body, Patch, Param, Delete ,Res} from '@nestjs/common';
import { OrderItemService } from './order-item.service';
import { CreateOrderItemDto } from './dto/create-order-item.dto';
import { UpdateOrderItemDto } from './dto/update-order-item.dto';
import { Response } from 'express';

@Controller('order-item')
export class OrderItemController {
  constructor(private readonly orderItemService: OrderItemService) {}

  @Post()
  create(@Body() createOrderItemDto: CreateOrderItemDto) {
    return this.orderItemService.create(createOrderItemDto);
  }

  @Get()
  findAll() {
    return this.orderItemService.findAll();
  }

  @Get('pdf')
  async getOrdersPdf(@Res() res: Response) {
    const pdfBuffer = await this.orderItemService.generatePdf();
    res.setHeader('Content-Type', 'application/pdf');
    res.setHeader('Content-Disposition', 'attachment; filename=orders-item.pdf');
    res.send(pdfBuffer);
  }

  // @Get('/:userId/pdf')
  // async generatePDF(@Param('userId') userId: number, @Res() res: Response) {
  //   try {
  //     const pdfBuffer = await this.orderItemService.generatePDF(userId);
  //     res.set({
  //       'Content-Type': 'application/pdf',
  //       'Content-Disposition': `attachment; filename=orders_${userId}.pdf`,
  //       'Content-Length': pdfBuffer.length,
  //     });
  //     res.end(pdfBuffer);
  //   } catch (error) {
  //     res.status(500).send({ message: 'Could not generate PDF' });
  //   }
  }
  // @Get('/:userId')
  // async getOrderItemsByUserId(@Param('userId') userId: number) {
  //   return this.orderItemService.getOrderItemsByUserId(userId);
  // }


  

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.orderItemService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateOrderItemDto: UpdateOrderItemDto) {
  //   return this.orderItemService.update(+id, updateOrderItemDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.orderItemService.remove(+id);
  // }

