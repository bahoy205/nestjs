import { PartialType } from '@nestjs/mapped-types';
import { CreateProductDto } from './create-product.dto';

export class UpdateProductDto extends PartialType(CreateProductDto) {
  productName: string; // Making productName optional
  amount: number;
  price: number;
  productTypeI: number;
}
