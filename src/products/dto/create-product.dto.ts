export class CreateProductDto {
    productName: string;
    amount: number;
    price: number;
    productTypeId: number;
  }
  