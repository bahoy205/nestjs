import { Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToOne, JoinColumn } from 'typeorm';
import { OrderItem } from 'src/order-item/entities/order-item.entity';
import { ProductType } from 'src/product-types/entities/product-type.entity';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  productId: number;

  @Column()
  productName: string;

  @Column()
  amount: number;

  @Column()
  price: number;

  @ManyToOne(() => ProductType)
  @JoinColumn({ name: 'productTypeId' })
  productType: ProductType;

  @Column()
  productTypeId: number;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.product)
  orderItems: OrderItem[];
}
