import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Product } from './entities/product.entity'; 
import { CreateProductDto } from './dto/create-product.dto'; 
import { ProductType } from '../product-types/entities/product-type.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
    @InjectRepository(ProductType)
    private readonly productTypeRepository: Repository<ProductType>,
  ) {}

  async create(createProductDto: CreateProductDto): Promise<Product> {
    const productType = await this.productTypeRepository.findOne({
      where: { productTypeId: createProductDto.productTypeId },
    });
  
    if (!productType) {
      throw new NotFoundException(`ProductType with ID ${createProductDto.productTypeId} not found`);
    }
  
    const product = this.productRepository.create({
      ...createProductDto,
      productType,
    });
  
    return this.productRepository.save(product);
  }
  

  async findAll(): Promise<Product[]> {
    return this.productRepository.find({ relations: ['productType'] });
  }

  async findOne(productId: number): Promise<Product> {
    const product = await this.productRepository.findOne({ where: { productId }, relations: ['productType'] });

    if (!product) {
      throw new NotFoundException(`Product with ID ${productId} not found`);
    }
    return product;
  }

  async update(id: number, updateProductDto: CreateProductDto): Promise<Product> {
    const product = await this.findOne(id);
    const productType = await this.productTypeRepository.findOne({
      where: { productTypeId: updateProductDto.productTypeId },
    });

    if (!productType) {
      throw new NotFoundException(`ProductType with ID ${updateProductDto.productTypeId} not found`);
    }

    product.productName = updateProductDto.productName;
    product.amount = updateProductDto.amount;
    product.price = updateProductDto.price;
    product.productType = productType;

    return this.productRepository.save(product);
  }

  async remove(id: number): Promise<void> {
    const result = await this.productRepository.delete(id);
    if (result.affected === 0) {
      throw new NotFoundException(`Product with ID ${id} not found`);
    }
  }
}
