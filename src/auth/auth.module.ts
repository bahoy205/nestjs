import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { UsersModule } from '../users/users.module';
import { JwtModule } from '@nestjs/jwt';
// import { jwtConstants } from './auth.constants'; // Import jwtConstants
import { UsersService } from '../users/users.service'; // Import UsersService

@Module({
  imports: [
    UsersModule,
    JwtModule.register({
      global: true,
      secret: process.env.JWT_SECRET_KEY, // Use jwtConstants.secret here
      signOptions: { expiresIn: '30m' },
    }),
  ],
  providers: [
    AuthService,
    UsersService, // Add UsersService to providers
  ],
  controllers: [AuthController],
  exports: [AuthService],
})
export class AuthModule {}
