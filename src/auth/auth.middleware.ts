import { Injectable, NestMiddleware, UnauthorizedException } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken'; // นำเข้า jwt มาใช้งาน

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    const authHeader = req.headers['authorization'];
    if (authHeader && authHeader.startsWith('Bearer ')) {
      const token = authHeader.substring(7); // เอาแค่ส่วนหลัง 'Bearer '
      try {
        req['userId'] = this.extractUserIdFromToken(token); // แก้ไขเรียกใช้ฟังก์ชัน extractUserIdFromToken
      } catch (error) {
        throw new UnauthorizedException('Invalid token');
      }
    } else {
      throw new UnauthorizedException('Authorization header missing or invalid');
    }
    next();
  }

  private extractUserIdFromToken(token: string): number {
    const decodedToken = this.decodeJwtToken(token); // ต้องแทนที่ด้วยการ decode token ของคุณ
    if (decodedToken && decodedToken.sub) { // Assuming 'sub' contains userId
      return decodedToken.sub;
    } else {
      throw new UnauthorizedException('Invalid token');
    }
  }

  private decodeJwtToken(token: string): any {
    try {
      const decodedToken = jwt.verify(token, process.env.JWT_SECRET_KEY); // ต้องแทนที่ด้วย secret key ของคุณ
      return decodedToken;
    } catch (error) {
      throw new UnauthorizedException('Invalid token');
    }
  }
}