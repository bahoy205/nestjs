import { ExecutionContext, CanActivate, UnauthorizedException, Injectable } from '@nestjs/common';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private readonly authService: AuthService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const token = request.headers.authorization?.split(' ')[1]; // ตรวจสอบ token

    if (!token) {
      throw new UnauthorizedException('Missing token');
    }

    try {
      const userId = await this.authService.getUserIdFromToken(token);
      if (!userId) {
        throw new UnauthorizedException('Invalid token');
      }
      
      request.user = { userId };
      return true;
    } catch (error) {
      throw new UnauthorizedException('Invalid or expired token');
    }
  }
}
