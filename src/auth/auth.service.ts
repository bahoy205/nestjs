import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async signIn(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findOneByUsername(username);
    if (!user || user.password !== pass) {
      throw new UnauthorizedException('Invalid username or password');
    }
    const { password, ...result } = user;
    const payload = { username: user.username, sub: user.userId, roles: user.role };
    
    // Ensure the secret key is set
    const secretKey = process.env.JWT_SECRET_KEY;
    if (!secretKey) {
      throw new UnauthorizedException('JWT secret key is not set');
    }

    return {
      access_token: this.jwtService.sign(payload, { secret: secretKey }),
      user: result,
    };
  }

  async getUserIdFromToken(token: string): Promise<number> {
    try {
      const secretKey = process.env.JWT_SECRET_KEY as string;

      if (!secretKey) {
        throw new UnauthorizedException('Secret key not provided');
      }

      const decodedToken: any = jwt.verify(token, secretKey);

      if (typeof decodedToken.sub !== 'number') {
        throw new UnauthorizedException('Invalid token payload');
      }

      return decodedToken.sub;
    } catch (error) {
      throw new UnauthorizedException('Invalid token');
    }
  }
}